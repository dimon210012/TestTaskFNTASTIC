// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Components/SplineComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "Animal.generated.h"

UCLASS()
class GALCHENKOFNTASTIC_API AAnimal : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AAnimal();
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Hierarchy")
	UParticleSystemComponent* ParticleSpawn;
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Hierarchy")
	UParticleSystemComponent* ParticleMove;

	UPROPERTY(BlueprintReadOnly, Category = "Reference")
	class USplineComponent* SplineRefObject;
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void Start(USplineComponent* SplineRef);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void Move(float AlphaTimeline);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void Finish();
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
