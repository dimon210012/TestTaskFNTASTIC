// Fill out your copyright notice in the Description page of Project Settings.


#include "PlatformButton.h"

// Sets default values
APlatformButton::APlatformButton()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>("RootActor");
	Root->SetupAttachment(RootComponent);
		Stand = CreateDefaultSubobject<UStaticMeshComponent>("StandMesh");
		Stand->SetupAttachment(Root);
			Button = CreateDefaultSubobject<UStaticMeshComponent>("ButtonMesh");
			Button->SetupAttachment(Stand);
				Finger = CreateDefaultSubobject<USceneComponent>("LocationFinger");
				Finger->SetupAttachment(Button);
				WidgetInteract = CreateDefaultSubobject<UWidgetComponent>("WidgetInteract");
				WidgetInteract->SetupAttachment(Button);
		SpawnPlatform = CreateDefaultSubobject<UStaticMeshComponent>("SpawnPlatformMesh");
		SpawnPlatform->SetupAttachment(Root);
			Spline = CreateDefaultSubobject<USplineComponent>("SplinePath");
			Spline->SetupAttachment(SpawnPlatform);
		BoxTrigger = CreateDefaultSubobject<UBoxComponent>("BoxTrigger");
		BoxTrigger->SetupAttachment(Root);
}

// Called when the game starts or when spawned
void APlatformButton::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APlatformButton::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

//Spawning the selected object and calling its logic
void APlatformButton::Spawn_Implementation()
{
	if((!bDoOnce) && (SpawnClass))
	{
		bDoOnce = true;
		
		FVector PointLoc(Spline->GetLocationAtSplinePoint(0, ESplineCoordinateSpace::World));
		FActorSpawnParameters SpawnInfo;
		SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		
		AAnimal* SpawnRef = GetWorld()->SpawnActor<AAnimal>(SpawnClass, PointLoc, FRotator(0.0f, 0.0f, 0.0f), SpawnInfo);

		SpawnRef->SetActorEnableCollision(false);
		SpawnRef->ParticleSpawn->SetTemplate(Particle);
		SpawnRef->Start(Spline);

		//Delay. Simulate DoOnce
		FTimerHandle Handle;
		GetWorld()->GetTimerManager().SetTimer(Handle, [this]()
        {
            bDoOnce = false;
        }, 4, 1);
	}
}

