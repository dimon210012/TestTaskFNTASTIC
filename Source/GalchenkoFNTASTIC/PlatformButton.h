// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "Components/SplineComponent.h"
//#include "Particles/ParticleSystem.h"
//#include "Particles/ParticleEmitter.h"
#include "Animal.h"
#include "Components/WidgetComponent.h"
#include "PlatformButton.generated.h"

UCLASS()
class GALCHENKOFNTASTIC_API APlatformButton : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APlatformButton();

	UPROPERTY(BlueprintReadOnly, Category = "Hierarchy")
	class USceneComponent* Root;
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Hierarchy")
	class UStaticMeshComponent* Stand;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Hierarchy")
	class UStaticMeshComponent* Button;
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Hierarchy")
	class USceneComponent* Finger;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Hierarchy")
	class UWidgetComponent* WidgetInteract;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Hierarchy")
	class UStaticMeshComponent* SpawnPlatform;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Hierarchy")
	class USplineComponent* Spline;
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Hierarchy");
	class UBoxComponent* BoxTrigger;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Parametrs")
	TSubclassOf<AAnimal> SpawnClass;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Parametrs")
	UParticleSystem* Particle;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void Spawn();
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	bool bDoOnce;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
