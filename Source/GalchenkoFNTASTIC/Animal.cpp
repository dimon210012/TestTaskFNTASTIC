// Fill out your copyright notice in the Description page of Project Settings.


#include "Animal.h"

// Sets default values
AAnimal::AAnimal()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ParticleSpawn = CreateDefaultSubobject<UParticleSystemComponent>("VisibleParticle");
	ParticleSpawn->SetupAttachment(GetMesh());
	ParticleMove = CreateDefaultSubobject<UParticleSystemComponent>("MoveParticle");
	ParticleMove->SetupAttachment(GetMesh());

	GetMesh()->SetVisibility(false);
	ParticleSpawn->SetVisibility(false);
	ParticleMove->SetVisibility(false);
}

// Called when the game starts or when spawned
void AAnimal::BeginPlay()
{
	Super::BeginPlay();

	FTimerHandle Handle;
	GetWorld()->GetTimerManager().SetTimer(Handle, [this]()
	{
        GetMesh()->SetVisibility(true);
		ParticleMove->SetVisibility(true);
    }, 0.2, 1);
}

// Called every frame
void AAnimal::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AAnimal::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}


//Initialization and start of movement logic
void AAnimal::Start_Implementation(USplineComponent* SplineRef)
{
	SplineRefObject = SplineRef;
}

//Spline movement calculation
void AAnimal::Move_Implementation(float AlphaTimeline)
{
	this->SetActorLocationAndRotation(SplineRefObject->GetLocationAtTime(AlphaTimeline, ESplineCoordinateSpace::World, true),
                                        SplineRefObject->GetRotationAtTime(AlphaTimeline, ESplineCoordinateSpace::World, true));
}

//End of movement and object removal
void AAnimal::Finish_Implementation()
{
	GetMesh()->SetVisibility(false);
	ParticleMove->SetVisibility(false);
	FTimerHandle Handle;
	GetWorld()->GetTimerManager().SetTimer(Handle, [this]()
    {
        this->Destroy();
    }, 2, 1);
}

